/*!
 * Dan's JavaScript Library v0.1
 * http://igneosaur.co.uk/
 *
 * Copyright 2013 Danny Edwards
 * Released under the MIT license
 *
 * Date: Fri Aug 16 2013
 */
var Game = (function(parent, $) {
  "use-strict";

  var fn = (parent.ajax = parent.ajax || {});

  /* Private vars */
  var ACCESS_TOKEN,
    SIGNED_REQUEST,
    USER_ID,
    interval,
    FB_PROFILE_PIC_URL =
      "https://graph.facebook.com/{id}/picture?width=80&height=80",
    GRID_WIDTH = 4,
    $timer = $(".timer"),
    $moves = $(".moves"),
    completed = 0,
    guess = [],
    moves = 0,
    time = 0,
    stringTime;

  /* Public functions */
  // Initialise the game's FB stuff.
  fn.init = function() {
    FB.getLoginStatus(function(response) {
      if (response.status === "connected") {
        // Already logged in and authenticated, so let's go!
        processAndBuild(response);
      } else {
        // Lets' login.
        FB.login(function(response) {
          if (response.authResponse) {
            // Assign all the vars.
            processAndBuild(response);
          } else {
            loginFail();
          }
        });
      }
    });
  };

  // On a click of the tile.
  fn.tileClick = function(tile) {
    var $tile = $(tile);

    if (guess.length !== 2 && !$tile.is(".flipped, .completed")) {
      guess.push($tile.data("userId"));
      $tile.addClass("flipped");

      if (guess.length === 2) {
        setTimeout(function() {
          if (guess[0] === guess[1]) {
            // Correct guess!
            $(".tile.flipped")
              .removeClass("flipped")
              .addClass("completed");
            completed++;
          }

          moves++;
          guess = [];

          $moves.html(moves);
          $(".tile").removeClass("flipped");
          checkCompleted();
        }, 500);
      }
    }
  };

  /* Private functions */
  // Process and build game
  var processAndBuild = function(response) {
    //assignAuthVars(response);
    getFriends(function(friends) {
      buildGrid(friends);
    });
  };

  // Assign all the variables.
  var assignAuthVars = function(response) {
    ACCESS_TOKEN = response.authResponse.accessToken;
    SIGNED_REQUEST = response.authResponse.signedRequest;
    USER_ID = response.authResponse.userID;
  };

  // Do something if the login fails.
  var loginFail = function() {
    console.warn("User cancelled login or did not fully authorize.");
  };

  // Get friends.
  var getFriends = function(callback) {
    var friendsRequired = (GRID_WIDTH * GRID_WIDTH) / 2,
      friends = [];

    FB.api("/me/invitable_friends", function(response) {
      var numFriends = response.data.length;

      for (i = 0; i < friendsRequired; i++) {
        var rand = Math.floor(Math.random() * numFriends),
          friend = response.data[rand];

        friends.push(friend);
      }

      callback(friends);
    });
  };

  // Build the grid.
  var buildGrid = function(friends) {
    var $table = $(".game-table tbody"),
      count = 0;

    friends = shuffleAndRandomise(friends);

    $table.html("");

    for (i = 0; i < GRID_WIDTH; i++) {
      var $currentRow = $("<tr>");
      $table.append($currentRow);

      for (j = 0; j < GRID_WIDTH; j++) {
        var friend = friends[count];
        // Clone the template.
        var tileTemplate = $("#tile-template")
          .clone()
          .html();
        var $tileTemplate = $($.trim(tileTemplate));
        // Populate the template.
        $tileTemplate.attr("id", i + "-" + j).data("userId", friend.id);
        $tileTemplate.find(".front").attr("src", friend.picture.data.url);
        $tileTemplate.find(".done").html(friend.name);

        $currentRow.append($tileTemplate);
        count++;
      }
    }

    finishLoading();
  };

  // Shuffles and randomises the friends array.
  var shuffleAndRandomise = function(friends) {
    var doubleList = friends.concat(friends);

    // Nicked this from css-tricks.com 'cause I was running out of time! ;)
    doubleList.sort(function() {
      return 0.5 - Math.random();
    });

    return doubleList;
  };

  // Checks if all the tiles are completed.
  var checkCompleted = function() {
    if (completed === (GRID_WIDTH * GRID_WIDTH) / 2) {
      $(".stats").html(
        "<h2>You did it in " + stringTime + " and " + moves + " moves! ;)</h2>"
      );
      stopTimer(interval);
    }
  };

  // Starts the timer.
  var startTimer = function() {
    var minutes, seconds;

    interval = setInterval(function() {
      time++;

      minutes = Math.floor(time / 60);
      seconds = time - minutes * 60;

      if (seconds < 10) {
        stringTime = minutes + ":0" + seconds;
      } else {
        stringTime = minutes + ":" + seconds;
      }

      $timer.html(stringTime);
    }, 1000);
  };

  var stopTimer = function(interval) {
    clearInterval(interval);
  };

  // Finished loading.
  var finishLoading = function() {
    $(".loading").remove();
    $(".stats").show();
    startTimer();
  };

  return fn;
})(Game || {}, jQuery);
